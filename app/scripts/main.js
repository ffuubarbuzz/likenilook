'use strict';
(function() {
	svg4everybody();
	let $ = document.querySelector.bind(document);
	let $$ = document.querySelectorAll.bind(document);

	WebFont.load({
		google: {
			families: ['Open Sans:300,400,400i,700,800,800i:latin,cyrillic']
		}
	});

	if (!Sticky.isFeatureSupported()) {
		Sticky.autoInit({
			selector: '.js-sticky'
		});
	}

	let dialogs = $$('dialog');
	Array.prototype.forEach.call(dialogs, dialog => {
		dialogPolyfill.registerDialog(dialog);
	});

    let anchorLinks = $$('a[href^="#"]:not([href="#"])');
	Array.prototype.forEach.call(anchorLinks, link => {
		let targetSelector = link.getAttribute('href').replace('#', '');
		CSS && CSS.escape && (targetSelector = CSS.escape(targetSelector));
		let target = $('#' + targetSelector);
		target && link.addEventListener('click', event => {
			let currentScroll = window.pageYOffset;
			event.preventDefault();
			window.location.href = link.href;
			window.scrollTo(window.pageXOffset, currentScroll)
			if (!target.offsetParent) {
				setTimeout(() => {
					target.scrollIntoView({ behavior: 'smooth' });
				}, 0);
			} else {
				target.scrollIntoView({ behavior: 'smooth' });
			}
		});
    });

	//TABS
	let tabToggles = $$('.js-tab-link[data-tabs]');
	let tabSlides = $$('.js-tab[data-tabs]');
	let tabGroups = [].map.call(tabToggles, toggle => {
		return toggle.dataset.tabs;
	});
	tabGroups = tabGroups.filter((value, index, self) => {
		return self.indexOf(value) === index;
	});
	Array.prototype.forEach.call(tabToggles, toggle => {
		let groupToggles = [].filter.call(tabToggles, tabToggle => {
			return tabToggle.dataset.tabs === toggle.dataset.tabs;
		});
		let currentClass = toggle.dataset.tabCurrentClass;
		let targetSlideSelector = toggle.dataset.tabId;
		let tabGroupSelector = `.js-tab-group[data-tabs=${toggle.dataset.tabs}]`;
		let targetSlide = $('#' + targetSlideSelector);
		let tabGroup = $(tabGroupSelector);
		toggle.addEventListener('click', event => {
			let index = 0;
			let e = targetSlide;
			if (currentClass) {
				Array.prototype.forEach.call(groupToggles, groupToggle => {
					groupToggle.classList.remove(currentClass);
				});
				toggle.classList.add(currentClass);
			}
			while ((e = e.previousElementSibling)) {
				index++;
			}
			let offset = index * -100;
			tabGroup.style.transform = 'translateX(' + offset + '%)';
		});
	});

	//SLIDES
	let sliders = $$('.js-slider');
	function changeSlide(slider, index) {
		let {slides, dots, prev, next, disabledClass} = slider;
		if (index < 0 || index > slides.length - 1) {
			return;
		}
		if (dots.length) {
			let currentDot = dots.item(index);
			Array.prototype.forEach.call(dots, dot => {
				dot.classList.remove(dot.dataset.activeClass);
			});
			currentDot.classList.add(currentDot.dataset.activeClass);
		}
		if (prev) {
			if (index === 0) {
				prev.classList.add(disabledClass);
			} else {
				prev.classList.remove(disabledClass);
			}
		}
		if (next) {
			if (index === slides.length - 1) {
				next.classList.add(disabledClass);
			} else {
				next.classList.remove(disabledClass);
			}
		}
		for (let k = slides.length - 1; k >= 0; k--) {
			let slide = slides[k];
			let offset = index * -100;
			slide.style.transform = 'translateX(' + offset + '%)';
			slide.classList.remove('js-current');
		}
		return index;
	}
	function setSliderAnimationInterval(slider) {
		if (slider.isAutoScrolled) {
			slider.timer = setInterval(() => {
				let index = ++slider.currentSlideIndex;
				index = index > slider.slides.length - 1 ? 0 : index;
				slider.currentSlideIndex = changeSlide(slider, index);
			}, slider.scrollInterval);
		}
	}
	Array.prototype.forEach.call(sliders, sliderElem => {
		let slider = {
			element: sliderElem,
			prev: sliderElem.querySelector('.js-prev'),
			next: sliderElem.querySelector('.js-next'),
			dots: sliderElem.querySelectorAll('.js-jump'),
			slides: sliderElem.querySelectorAll('.js-slide'),
			isAutoScrolled: sliderElem.dataset.sliderAutoScroll !== undefined,
			scrollInterval: parseInt(sliderElem.dataset.sliderScrollInterval, 10) || 5000,
			disabledClass: sliderElem.dataset.disabledClass,
			currentSlideIndex: 0,
		};
		slider.slides = Array.prototype.filter.call(slider.slides, slide => {
			// Filtering nested sliders
			let elem = slide.parentElement;
			while (elem) {
				if (elem === sliderElem) {
					break;
				}
				if (elem.classList.contains('js-slider')) {
					return false;
				}
				elem = elem.parentElement;
			}
			return true;
		});
		slider.element.addEventListener('mouseover', event => {
			clearInterval(slider.timer);
		});
		slider.element.addEventListener('mouseleave', event => {
			setSliderAnimationInterval(slider);
		});
		slider.prev.addEventListener('click', event => {
			event.preventDefault();
			changeSlide(slider, --slider.currentSlideIndex);
		});
		slider.next.addEventListener('click', event => {
			event.preventDefault();
			changeSlide(slider, ++slider.currentSlideIndex);
		});
		Array.prototype.forEach.call(slider.dots, (dot, index) => {
			dot.addEventListener('click', event => {
				event.preventDefault();
				changeSlide(slider, index);
			});
		});
		setSliderAnimationInterval(slider);
	});

	//TOGGLES
	let toggles = $$('.js-toggle');
	let dismissables = [];
	Array.prototype.forEach.call(toggles, toggle => {
		let target = toggle.dataset.toggleTarget;
		let className = toggle.dataset.toggleClass;
		let attribute = toggle.dataset.toggleAttribute;
		let action = toggle.dataset.toggleAction;
		let isDismissable = typeof(toggle.dataset.toggleDismissable) !== 'undefined';
		let dismissable = {};
		CSS && CSS.escape && (target = CSS.escape(target));
		target = $(`#${target}`);
		action = action || 'toggle';
		if (!target || !(className || attribute)) {
			console.warn('Something wrong with toggle', toggle)
			return;
		}
		if (isDismissable) {
			dismissable.element = target;
			if (className) dismissable.className = className;
			if (attribute) dismissable.attribute = attribute;
			dismissables.push(dismissable);
		}
		toggle.addEventListener('click', event => {
			if (event.target === target) {
				return;
			}
			event.stopPropagation();
			if (toggle.tagName === 'A') {
				event.preventDefault();
			}
			dismissToggleTargets(dismissables, target);
			if (className) {
				target.classList[action](className);
			}
			if (attribute && target.getAttribute(attribute) !== null) {
				target.removeAttribute(attribute);
			} else if (attribute) {
				target.setAttribute(attribute, '');
			}
		});
		target.addEventListener('click', event => { event.stopPropagation(); });
	});

	function dismissToggleTargets(targets, except) {
		Array.prototype.forEach.call(targets, dismissable => {
			let {element, className, attribute} = dismissable;
			if (except) {
				if (Array.isArray(except) && except.indexOf(element) !== -1) {
					return;
				}
				if (except === element) {
					return;
				}
			}
			if (className) {
				element.classList.remove(className);
			}
			if (attribute && element.getAttribute(attribute) !== null) {
				element.removeAttribute(attribute);
			}
		});
	}
	document.addEventListener('click', event => {
		dismissToggleTargets(dismissables);
	});

	//PRODUCTS
	const products = $$('.js-product');
	Array.prototype.forEach.call(products, product => {
		const img = product.querySelector('img');
		img && img.addEventListener('error', () => {
			const imagelessClass = product.dataset.imagelessClass;
			imagelessClass && product.classList.add(imagelessClass);
			img.parentElement && img.parentElement.removeChild(img);
		});
	});

	//MODALS
	let modalTriggers = $$('.js-modal-trigger');
	Array.prototype.forEach.call(modalTriggers, trigger => {
		let dialogId = trigger.dataset.modal;
		CSS && CSS.escape && (dialogId = CSS.escape(dialogId));
		let dialog = $('#' + dialogId);
		let fromDialog = false;
		let elem = trigger.parentElement;
		while (elem) {
			if (elem.tagName === 'DIALOG') {
				fromDialog = elem;
				break;
			}
			elem = elem.parentElement;
		}
		trigger.addEventListener('click', () => {
			if (fromDialog) {
				fromDialog.close();
			}
			if (fromDialog === dialog) {
				return;
			}
			if (dialog.open) {
				dialog.close();
			} else {
				dialog.showModal();
			}
		});
	});

	let modals = $$('.js-modal');
	Array.prototype.forEach.call(modals, modal => {
		modal.addEventListener('click', event => {
			if (!modal.open) {
				return;
			}
			if (modal.contains(event.target) && event.target !== modal) {
				return;
			}
			let dialogRect = modal.getBoundingClientRect();
			let x = event.pageX;
			let y = event.pageY;
			let left = window.pageXOffset + dialogRect.left;
			let top = window.pageYOffset + dialogRect.top;
			let rect = {
				left: left,
				right: left + dialogRect.width,
				top: top,
				bottom: top + dialogRect.height
			};
			if (!(x >= rect.left && x <= rect.right
			    && y >= rect.top && y <= rect.bottom)) {
				modal.close();
			}
		});
	});

	// ClICK FAKERS
	let clikTriggers = $$('.js-click-trigger');
	Array.prototype.forEach.call(clikTriggers, trigger => {
		trigger.addEventListener('click', event => {
			let targetId = trigger.dataset.clickTarget;
			CSS && CSS.escape && (targetId = CSS.escape(targetId));
			if (!targetId) {
				return;
			}
			let target = $('#' + targetId);
			target.click();
		});
	});

	//AUTH FORMS
	let authForms = $$('.js-auth-form');
	Array.prototype.forEach.call(authForms, authForm => {
		let fields = $$('.js-auth-field');
		let errorFieldClass = authForm.dataset.errorFieldClass;
		let dirtyFieldClass = authForm.dataset.dirtyFieldClass;
		Array.prototype.forEach.call(fields, field => {
			let input = field.querySelector('input');
			let errorMessage = field.querySelector('.js-auth-field-error-message');
			if (!input) {
				return;
			}
			input.addEventListener('input', event =>  {
				field.classList.remove(errorFieldClass);
				if (errorMessage) {
					errorMessage.parentElement.removeChild(errorMessage);
				}
				if (input.value === '') {
					field.classList.remove(dirtyFieldClass);
				} else {
					field.classList.add(dirtyFieldClass);
				}
			});
		});
	});

	//TOGGLES
	let expandToggles = $$('.js-expand-list');
	Array.prototype.forEach.call(expandToggles, toggle => {
		let target = toggle.dataset.expandTarget;
		let className = toggle.dataset.expandClass;
		target = $(`#${target}`);
		toggle.addEventListener('click', event => {
			target.classList.add(className);
		});
	});
})();

(function() {
	//SELECT2
	let $selects = $('.js-select2');
	$selects.each((index, select) => {
		let $select = $(select);
		let className = $select.attr('data-select2-class')
		$select.select2({
			containerCssClass: className
		});
	});

	//FILTERABLES
	const $filterables = $('.js-filterable');
	$filterables.each((index, filterable) => {
		let $input = $(filterable).find('input[type="search"]');
		let $itemTemplate = $(filterable).find('.js-filterable-item');
		let $optionsContainer = $(filterable).find('.js-filterable-options');
		let url = filterable.dataset.filterableUrl;
		let prefix = filterable.dataset.filterablePrefix;
		let loadingClass = filterable.dataset.filterableLoadingClass;
		let queryTimer;
		if (!$input.length ||
		    !$itemTemplate.length ||
		    !$optionsContainer.length ||
		    !url ||
		    !prefix ||
		    !loadingClass) {
			return;
		}
		let $template = $($itemTemplate.prop('content'));
		$input.on('input', function(event) {
			let input = this;
			$optionsContainer.addClass(loadingClass);
			clearTimeout(queryTimer);
			queryTimer = setTimeout(() => {
				let query = input.value;
				$.get(url, {
					query: query
				}).done(data => {
					$optionsContainer.empty();
					let parsedData = Array.isArray(data) ? data : JSON.parse(data);
					parsedData.forEach(option => {
						let id = CSS.escape(prefix + option);
						$template.find('.js-filterable-checkbox').attr('id', id);
						$template.find('.js-filterable-label').attr('for', id).text(option);
						$optionsContainer.append(document.importNode($template[0], true));
					});
				}).always(() => {
					$optionsContainer.removeClass(loadingClass);
				});
			}, 300);
		});
	});
})();
