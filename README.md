# likenilook markup

## Under the hood
- [Yeoman generator web app](https://github.com/yeoman/generator-webapp)
- [Gulp-svgstore](https://github.com/w0rm/gulp-svgstore)
- [Pug](https://pugjs.org/api/getting-started.html)

## Getting started

- Install Node.js
- Install CLI dependencies: `npm install --global gulp-cli bower`
- Install dev dependencies: `npm install`
- Install product dependencies: `bower install`

## Working with project

- Run `gulp` to build product for production
- Run `gulp serve` to preview and watch for changes
- Run `gulp serve:dist` to preview the production build
- Run `bower install --save <package> && gulp wiredep` to install frontend dependencies
